const { body, validationResult } = require("express-validator");

async function successfulRequest(payload) {
  return { success: true, payload, error: null };
}

async function failedRequest(error) {
  return { success: false, payload: null, error };
}

async function emailValidator(req, res) {
  //express validator for validating email
  req.body.hasOwnProperty(email).isEmail();
  req.body.hasOwnProperty(password).isLength({ min: 5 });
  body("username").isEmail(),
    // password must be at least 5 chars long
    body("password").isLength({ min: 5 }),
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
    };
  //conditions to check if all fields are not empty
  if (
    !req.body.hasOwnProperty(name) ||
    !req.body.hasOwnProperty(email) ||
    !req.body.hasOwnProperty(password)
  ) {
    return "Fill in required details";
  }
}

module.exports = {
  successfulRequest,
  failedRequest,
  emailValidator,
};
