const router = require("express").Router();
const userController = require("../controller/userControllers");
const authControllers = require("../controller/authControllers");
const { Router } = require("express");

// router.post("/signup", authControllers);
router.post("/register", async (req, res) => {
  try {
    const controllerLogic = await userController.createNewUser(req, res);
    return res.status(200).json(controllerLogic);
  } catch (error) {
    return res.status(500).json(error.message);
  }
});

router.get("/register", async (req, res) => {
  try {
    const controllerLogic = await userController.allUser(req, res);
    return res.status(200).json(controllerLogic);
  } catch (error) {
    return res.status(500).json(error.message);
  }
});

router.get("/register/:id", async (req, res) => {
  const controllerLogic = await userController.oneUser(req, res);
  return res.status(200).json(controllerLogic);
});

router.post("/login", async (req, res) => {
  try {
    const controllerLogic = await userController.loginUser(req, res);
    res.status(200).json(controllerLogic);
  } catch {
    res.status(500).json(error.message);
  }
});

module.exports = router;
