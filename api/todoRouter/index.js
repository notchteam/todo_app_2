const router = require("express").Router();

const todoRoutes = require("./todoRouter");
const userRoutes = require("./userRouter");

router.use("/api/todo", todoRoutes);
router.use("/user", userRoutes);

module.exports = router;
