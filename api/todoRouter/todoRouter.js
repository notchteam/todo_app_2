const router = require("express").Router();
const todoController = require("../controller/todoControllers");
const upload = require("../../helpers/multer");

router.get("/", async (req, res) => {
  const controllerLogic = await todoController.getTodo(req, res);
  return res.status(200).json(controllerLogic);
});

router.get("/:id", async (req, res) => {
  let controllerLogic = await todoController.getOneTodo(req, res);
  return res.status(200).json(controllerLogic);
});

router.post("/", upload.single("image"), async (req, res) => {
  const controllerLogic = await todoController.createTodo(req, res);
  return res.status(201).json(controllerLogic);
});

router.put("/:id", async (req, res) => {
  const controllerLogic = await todoController.updateTodo(req, res);
  return res.status(200).json(controllerLogic);
});

router.delete("/:id", async (req, res) => {
  const controllerLogic = await todoController.deleteTodo(req, res);
  return res.status(200).json(controllerLogic);
});

module.exports = router;
