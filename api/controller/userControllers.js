const uuid = require("uuid");
const { User } = require("../../database/connection");
const { successfulRequest, failedRequest } = require("../../helpers/responses");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

class UserController {
  static async createNewUser(req, res) {
    try {
      let { name, email, password } = req.body;
      let id = uuid.v4();
      if (
        req.body.hasOwnProperty("name") &&
        req.body.hasOwnProperty("email") &&
        req.body.hasOwnProperty("password")
      ) {
        let hash = await bcrypt.hash(password, 10);
        let response = await User.create({
          id,
          name,
          email,
          password: hash,
        });
        let token = await jwt.sign({ id }, "secretKey@atb1234567890");
        //const result = await successfulRequest(response);

        return { success: true, payload: response, token, error: null };
      } else {
        return "Fill in required details";
      }
    } catch (err) {
      const error = await failedRequest(err.message);
      return error;
    }
  }

  static async loginUser(req, res) {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({
        limit: 1,
        first: "*",
        where: { email: email },
      });
      if (user) {
        console.log("users");
        console.log(user);
        console.log(User.password);
        const validPass = await bcrypt.compare(password, user.password);
        console.log("valid pass interface");
        console.log(validPass);
        if (validPass) {
          let token = await jwt.sign(
            { id: user.id },
            "secretKey@atb1234567890"
          );
          res.json({ message: "successful login", token });
        } else {
          res.status(404).json({ message: "wrong username or password" });
        }
      }
      // const id = await uuid.v4();
      // const response = User.create({
      //   id,
      //   name,
      //   email,
      //   password,
      // });
      // const result = await successfulRequest(response);
      // return result;
    } catch (err) {
      const error = await failedRequest(err.message);
      return error;
    }
  }

  static async allUser(req, res) {
    try {
      const response = await User.findAll({
        order: [["createdAt", "DESC"]],
      });
      const result = successfulRequest(response);
      return result;
    } catch (err) {
      const result = failedRequest(err.message);
      return result;
    }
  }

  static async oneUser(req, res) {
    try {
      const id = req.params.id;
      const response = await User.findOne({
        where: {
          id,
        },
      });
      const result = successfulRequest(response);
      return result;
    } catch (err) {
      const result = failedRequest(err.message);
      return result;
    }
  }
}

module.exports = UserController;
