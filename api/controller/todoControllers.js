const { Todo } = require("../../database/connection");
const uuid = require("uuid");
const {
  successfulRequest,
  failedRequest,
  emailValidator,
} = require("../../helpers/responses");

class TodoController {
  static async getTodo(req, res) {
    try {
      const response = await Todo.findAll({
        order: [["createdAt", "DESC"]],
      });
      const result = successfulRequest(response);

      return result;
    } catch (err) {
      const result = await failedRequest(err.message);
      return result;
    }
  }

  static async getOneTodo(req, res) {
    try {
      const id = req.params.id;

      const response = await Todo.findOne({
        where: {
          id,
        },
      });
      console.log(response);
      const result = successfulRequest(response);
      console.log(result);
      return result;
    } catch (err) {
      const result = failedRequest(err.message);
      return result;
    }
  }

  static async createTodo(req, res) {
    try {
      const { title, description } = req.body;
      let image = req.file.path;
      const id = uuid.v4();
      //unsign header authorization (token)
      let response = await Todo.create({
        id,
        title,
        description,
        image,
      });
      const result = await successfulRequest(response);
      return result;
    } catch (err) {
      const result = failedRequest(err.message);
      return result;
    }
  }

  static async updateTodo(req, res) {
    try {
      const { title, description, image } = req.body;
      const id = req.params.id;
      console.log(req.body);
      let response = await Todo.update(
        { description, image, title },
        { where: { id } }
      );
      if (response[0] != 1) {
        return res.json({
          status: "Failed",
          message: "Nothing to Update",
        });
      }
      response = {
        id,
        description,
        image,
        title,
      };
      const result = await successfulRequest(response);
      return result;
    } catch (err) {
      const result = await failedRequest(err.message);
    }
  }

  static async deleteTodo(req, res) {
    try {
      const id = req.params.id;
      const response = await Todo.destroy({
        where: {
          id,
        },
      });
      const result = successfulRequest(response);
      return result;
    } catch (err) {
      const result = failedRequest(err.message);
      return result;
    }
  }
}

module.exports = TodoController;
