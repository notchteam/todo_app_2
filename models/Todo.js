module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "todos",
    {
      id: {
        type: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      description: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      image: {
        type: DataTypes.STRING,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      timestamps: true,
    }
  );
};

/** 
  Todo.belongsTo(User(sequelize, DataTypes));
  return Todo;
};
**/
