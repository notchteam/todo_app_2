const express = require("express");
const helmet = require("helmet");

const appRouter = require("./api/todoRouter/index");
const db = require("./database/connection");

const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

app.use("/", appRouter);

const PORT = 3000;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
