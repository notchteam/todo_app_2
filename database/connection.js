const Sequelize = require("sequelize");

const connection = new Sequelize("todo_db", "todo", "todopassword", {
  host: "atbdb-test.uksouth.cloudapp.azure.com",
  dialect: "mysql",
});

connection
  .authenticate()
  .then(() => {
    console.log("Connected to the Database");
  })
  .catch((err) => console.log(err));
let databaseModels = {};

databaseModels.Sequelize = Sequelize;
databaseModels.sequelize = connection;

databaseModels.User = require("../models/user")(connection, Sequelize);
databaseModels.Todo = require("../models/Todo")(connection, Sequelize);

module.exports = databaseModels;
